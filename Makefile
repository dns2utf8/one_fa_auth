HOST="web_1fa.estada.ch@yeti.estada.ch"
BINARY="one_fa_auth"
SERVICEFILE="one_fa.service"

deploy: build_release deploy_direct
	# done

deploy_direct:
	ssh ${HOST} -t "systemctl --user stop SERVICEFILE || true"
	rsync -avz --delete-after ./mail_config.json target/release/${BINARY} ./templates ./static ${HOST}:
	make systemd_restart

systemd_setup:
	ssh ${HOST} "mkdir -p ~/.config/systemd/user/"
	scp ${SERVICEFILE} ${HOST}:~/.config/systemd/user/${SERVICEFILE}
	ssh ${HOST} -t "systemctl --user daemon-reload ; systemctl --user restart ${SERVICEFILE} ; systemctl --user enable ${SERVICEFILE} ; systemctl --user status ${SERVICEFILE}"

systemd_stop:
	ssh ${HOST} -t "systemctl --user stop ${SERVICEFILE} ; systemctl --user status ${SERVICEFILE}"

systemd_status:
	ssh ${HOST} -t "systemctl --user status ${SERVICEFILE}"

systemd_restart:
	ssh ${HOST} -t "systemctl --user restart ${SERVICEFILE} ; systemctl --user status ${SERVICEFILE}"

touch_src:
	touch build.rs
	touch src/*.rs

build_release: touch_src
	cargo build --release

build_in_container_cached: touch_src
	docker pull rust:latest
	docker run --rm -it --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -v "$$HOME"./cargo/registry/:/usr/local/cargo/registry/:ro -w /usr/src/myapp rust:latest cargo build --release
	chown "$$(id -u)":"$$(id -g)" target/release/${BINARY}

build_in_container: touch_src
	docker pull rust:latest
	docker run --rm --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -w /usr/src/myapp rust:latest cargo build --release
	chown "$$(id -u)":"$$(id -g)" target/release/${BINARY}

ssh:
	ssh ${HOST}

ws_debug:
	websocat ws://localhost:8080/ws/