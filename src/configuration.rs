use crate::mail_config::{MailConfig, MAIL_CONFIG};
use actix_identity::Identity;
use log::{debug, error, info};
use mailgun_v3::{
    email::{async_impl::send_email, EmailAddress, Message, MessageBody, SendResponse},
    Credentials, MailgunResult,
};
use serde::{Deserialize, Serialize};
use std::{
    net::IpAddr,
    ops::{Deref, DerefMut},
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};

pub type Config = Arc<Mutex<ConfigRaw>>;
pub type WebConfig = actix_web::web::Data<crate::configuration::Config>;

pub const CONFIG_PATH: &str = "./config.json";

pub fn restore() -> std::io::Result<Config> {
    let state = match file_restore() {
        Ok(s) => s,
        Err(e) => {
            println!("Config::restore() -> {}\n    using default", e);
            let mut s = ConfigRaw {
                users: vec![],
                sessions: vec![],
            };
            s.store_to_disk()?;
            s
        }
    };

    Ok(Arc::new(Mutex::new(state)))
}
fn file_restore() -> Result<ConfigRaw, String> {
    let content = std::fs::read_to_string(CONFIG_PATH).map_err(|e| format!("{:?}", e))?;

    serde_json::from_str(&content).map_err(|e| format!("{:?}", e))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ConfigRaw {
    users: Vec<UserData>,
    sessions: Vec<UserSession>,
}

impl ConfigRaw {
    pub fn store_to_disk(&mut self) -> std::io::Result<()> {
        let data = //serde_json::to_string(self)
        serde_json::to_string_pretty(self)
            .expect("unable to serialize ConfigRaw");
        std::fs::write(CONFIG_PATH, data)
    }

    pub fn save_session(&mut self, session: UserSession) -> std::io::Result<()> {
        if let Some(s) = self.sessions.iter_mut().find(|us| us.id == session.id) {
            *s = session;
            s.last_activity = Instant::now();
            self.store_to_disk()?;
        } else {
            error!("ConfigRaw::save_session() unable to find {:?}", session);
        }
        Ok(())
    }

    fn cleanup_sessions(&mut self) {
        let now = Instant::now();
        const TIMEOUT: Duration = Duration::from_secs(60 * 60);
        self.sessions.retain(|us| now - us.last_activity < TIMEOUT);
    }

    pub fn session_or_default(&mut self, id: &Identity) -> UserSession {
        let candidate = self.session(id);
        self.session_candidate_or_new(id, candidate)
    }

    pub fn session(&mut self, id: &Identity) -> Option<UserSession> {
        self.cleanup_sessions();

        id.identity().and_then(|id| {
            let id = SessionId(id);
            self.sessions.iter_mut().find(|us| us.id == id).map(|us| {
                us.last_activity = Instant::now();
                us.clone()
            })
        })
    }

    pub fn session_from_confirm_token(
        &mut self,
        id: &Identity,
        token: &String,
    ) -> Option<UserSession> {
        self.cleanup_sessions();

        let email = self
            .users
            .iter()
            .find(|u| u.active_login_token.as_ref() == Some(token))
            .map(|ud| ud.email.clone())?;

        id.forget();
        let mut us = self.session_or_default(id);
        us.email = Some(email.clone());
        Some(us)
    }

    pub fn session_candidate_or_new(
        &mut self,
        id: &Identity,
        candidate: Option<UserSession>,
    ) -> UserSession {
        candidate.unwrap_or_else(|| {
            let us = UserSession::new_random(id);
            self.sessions.push(us.clone());

            // maybe check if it already exists?
            us
        })
    }

    pub fn create_user<S: AsRef<str>>(
        &mut self,
        email: S,
        session: &mut UserSession,
        registered_by: IpAddr,
    ) -> Result<(), ()> {
        let email = email.as_ref();
        if self.users.iter().any(|u| u.email == email) {
            error!("ConfigRaw::create_user({}) duplicate user prevented", email);
            return Err(());
        }

        let mut user = UserData {
            email: email.to_string(),
            state: UserState::Registered,
            activity: vec![(UserAction::Created, TsIp::now(registered_by))],
            active_login_token: None,
        };
        //session.email = Some(email.to_string());

        self.users.push(user);
        self.store_to_disk().map_err(|_| ())
    }
    pub fn get_user_data<S: AsRef<str>>(&self, email: S) -> Option<&UserData> {
        let email = email.as_ref();
        self.users.iter().filter(|u| u.email == email).next()
    }
    pub fn get_user_data_mut(
        &mut self,
        session: UserSession,
    ) -> Option<
        //&mut UserData
        SaveGuard,
    > {
        let email = session.email.as_ref()?;
        let user_offset = self
            .users
            .iter_mut()
            .enumerate()
            .filter(|(_, u)| u.email == *email)
            .map(|(o, _)| o)
            .next()?;
        Some(SaveGuard {
            users: self,
            user_offset,
            session,
        })
    }
    /*pub fn get_email_by_token(&self, token: Option<&String>) -> Option<String> {
        self.users
            .iter()
            .filter(|u| u.active_login_token.as_ref() == token)
            .map(|u| u.email.clone())
            .next()
    }*/
}

pub struct SaveGuard<'a> {
    users: &'a mut ConfigRaw,
    // user: &'a mut UserData,
    user_offset: usize,
    session: UserSession,
}

impl<'a> SaveGuard<'a> {
    pub fn complete_login(&mut self, by: IpAddr) {
        let ref mut user = self.users.users[self.user_offset];
        user.complete_login_raw(&mut self.session, by)
    }
    pub async fn start_login(&mut self, by: IpAddr) -> MailgunResult<SendResponse> {
        let ref mut user = self.users.users[self.user_offset];
        user.start_login_raw(&mut self.session, by, &MAIL_CONFIG)
            .await
    }
    pub fn logout(&mut self, by: IpAddr) {
        let ref mut user = self.users.users[self.user_offset];
        user.logout(&mut self.session, by)
    }
}

impl<'a> Drop for SaveGuard<'a> {
    fn drop(&mut self) {
        debug!("SaveGuard::drop() -> save_session({:?})", self.session);
        let mut session = self.session.clone();
        session.last_activity = Instant::now();
        self.users.save_session(session);
    }
}
/*impl<'a> Deref for SaveGuard<'a> {
    type Target =  UserData;
    fn deref(&mut self) -> & Self::Target {
        self.user
    }
}*/
impl<'a> Deref for SaveGuard<'a> {
    type Target = UserData;

    fn deref(&self) -> &UserData {
        &self.users.users[self.user_offset]
    }
}
impl<'a> DerefMut for SaveGuard<'a> {
    fn deref_mut(&mut self) -> &mut UserData {
        &mut self.users.users[self.user_offset]
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserData {
    pub active_login_token: Option<String>,
    pub email: String,
    pub state: UserState,
    activity: Vec<(UserAction, TsIp)>,
}

impl UserData {
    async fn start_login_raw(
        &mut self,
        session: &mut UserSession,
        by: IpAddr,
        mailconfig: &MailConfig,
    ) -> MailgunResult<SendResponse> {
        self.state = UserState::Registered;
        self.activity.push((UserAction::StartLogin, TsIp::now(by)));

        let token = format!("{:x}", rand::random::<u128>());
        let link = format!(
            "https://{}/me/confirm_email?token={}",
            mailconfig.creds.domain(),
            token
        );

        let receipient = EmailAddress::address(&self.email);
        let name = self.email.split('@').next().unwrap_or("");
        let subject = format!("New Login on {}", mailconfig.creds.domain());
        let body = format!(
            "Hi {}\n\nClick this link to access your account:\n{}\n\nCheers,\n1FA.estada.ch",
            name, link
        );

        let message = Message {
            to: vec![receipient],
            cc: vec![],
            bcc: vec![],
            subject,
            body: MessageBody::Text(body),
            options: vec![],
        };

        debug!(
            "UserData::start_login(by = {:?}) -> sending to {}",
            by, self.email
        );
        let r = send_email(&mailconfig.creds, &mailconfig.sender, message).await?;
        self.active_login_token = Some(token);
        debug!(
            "UserData::start_login(by = {:?}) -> token for {} activated",
            by, self.email
        );

        session.state = SessionState::EMailSent;
        Ok(r)
    }
    pub fn complete_login_raw(&mut self, session: &mut UserSession, by: IpAddr) {
        session.state = SessionState::LoggedIn;
        session.email = Some(self.email.clone());

        self.state = UserState::Verified;
        self.active_login_token = None;
        self.activity.push((UserAction::Login, TsIp::now(by)));
    }
    fn logout(&mut self, session: &mut UserSession, by: IpAddr) {
        session.state = SessionState::Unauthenticated;
        self.activity.push((UserAction::Logout, TsIp::now(by)));
    }

    pub fn activity_report(&self) -> Vec<(String, String)> {
        self.activity
            .iter()
            .rev()
            .map(|(what, ts)| {
                (
                    format!("{:?}", what),
                    format!(
                        "{} ago by {}",
                        compound_duration::format_wdhms(ts.when.elapsed().as_secs()),
                        ts.by
                    ),
                )
            })
            .collect()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum UserState {
    Registered,
    // maybe to fight spam
    //EMailSent(#[serde(with = "serde_millis")] Instant),
    Verified,
    //TotpSecret(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub enum UserAction {
    Created,
    StartLogin,
    EMailSent,
    Login,
    Logout,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TsIp {
    #[serde(with = "serde_millis")]
    when: Instant,
    by: IpAddr,
}
impl TsIp {
    pub fn now(by: IpAddr) -> TsIp {
        TsIp {
            when: Instant::now(),
            by,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct UserSession {
    pub id: SessionId,
    pub state: SessionState,
    pub email: Option<String>,
    #[serde(with = "serde_millis")]
    last_activity: Instant,
}

impl UserSession {
    fn new_random(id: &Identity) -> Self {
        let new = SessionId::random();
        info!("UserSession::new_random() -> new session {:?}", new);
        id.remember(new.0.clone());

        UserSession {
            id: new,
            email: None,
            state: SessionState::Unauthenticated,
            last_activity: Instant::now(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub enum SessionState {
    Unauthenticated,
    EMailSent,
    LoggedIn,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct SessionId(pub String);
impl SessionId {
    fn random() -> SessionId {
        let r: u128 = rand::random();
        SessionId(format!("{:x}", r))
    }
}
