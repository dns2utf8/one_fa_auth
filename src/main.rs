#![allow(unused)]

use actix::{Actor, StreamHandler};
use actix_files::NamedFile;
use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
//use actix_session::{CookieSession, Session};
use actix_web::{
    error, get, post, web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder,
};
use actix_web_actors::ws::{self, WebsocketContext};
use configuration::{Config, SaveGuard, SessionId, SessionState, UserSession, UserState};
//use drop_guard::DropGuard;
use log::{debug, error, info, warn};
use mail_config::{MailConfig, MAIL_CONFIG};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    net::{IpAddr, SocketAddr},
    path::PathBuf,
    time::Duration,
};
use tera::Tera;

mod configuration;
mod mail_config;
mod utils;

const GIT_VERSION: &str = env!("GIT_VERSION");

/*
/// Define HTTP actor
struct MyWs;

impl Actor for MyWs {
    type Context = WebsocketContext<Self>;
}

/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWs {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        use ws::Message;
        match msg {
            Ok(Message::Ping(msg)) => ctx.pong(&msg),
            Ok(Message::Text(text)) => ctx.text(text),
            Ok(Message::Binary(bin)) => ctx.binary(bin),
            _ => (),
        }
    }
}
async fn ws_start(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    let resp = ws::start(MyWs {}, &req, stream);
    println!("{:?}", resp);
    resp
}
*/

#[get("/")]
async fn index(
    id: Identity,
    tmpl: web::Data<tera::Tera>,
    users: configuration::WebConfig,
) -> Result<HttpResponse, Error> {
    let mut users = users.lock().expect("/index unable to lock users");
    let session = users.session_or_default(&id);

    let mut ctx = default_tera_context(&session);

    let s = tmpl.render("index.html", &ctx).map_err(|e| {
        warn!("index: {:?}", e);
        error::ErrorInternalServerError("Template error index.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[derive(Serialize, Deserialize)]
pub struct LoginParams {
    email: String,
}

async fn login(
    request: HttpRequest,
    id: Identity,
    tmpl: web::Data<Tera>,
    users: configuration::WebConfig,
    params: Option<web::Form<LoginParams>>,
    query: web::Query<HashMap<String, String>>,
) -> Result<HttpResponse, Error> {
    // Security so we don't get abused that easy by people that try to verify a users existence in the system
    let next = match query.get("next") {
        Some(n) if n.starts_with("/") => n,
        Some(_) | None => "/",
    };

    let mut users = users.lock().expect("/index unable to lock users");
    let mut session = users.session_or_default(&id);

    let mut ctx = default_tera_context(&session);
    let mut messages = vec![];

    if let Some(params) = params {
        let email = params.email.to_lowercase().trim().to_string();
        ctx.insert("user_email", &email);

        if email.is_empty() {
            messages.push("Empty email, not allowed!")
        } else if email.chars().filter(|c| *c == '@').count() != 1 {
            messages.push("Invalid email, must contain one @");
        } else if utils::is_allowed_domain(&email) {
            let client_ip = client_ip_from_request(&request);
            info!("login accepted: {:?} from {}", email, client_ip);
            session.email = Some(email.clone());

            if let Some(user) = users.get_user_data(&email) {
                // nothing to do here as we send the email link again if needed in the
                // /me/confirm_email page
                users.save_session(session);
            } else {
                match users.create_user(&email, &mut session, client_ip) {
                    Ok(_) => {
                        // ignore disk error here
                        users.save_session(session);
                    }
                    Err(e) => {
                        error!("/login unable to create user -> {:?}", e);
                        messages.push("unable to create user, please try again later");
                    }
                };
            }
        } else {
            messages.push("your email not in the allowed domains list! Currently: *.jku.at");
        }
    } else {
        messages.push("You must provide an email address!");
    }

    if messages.is_empty() {
        return Ok(HttpResponse::Found()
            .header("location", format!("/me/confirm_email?next={}", next))
            .finish());
    }

    debug!("/login -> reject atempt");
    ctx.insert("messages", &messages);
    ctx.insert("next", next);

    let s = tmpl.render("sign_in_first.html", &ctx).map_err(|e| {
        warn!("me::login: {:?}", e);
        error::ErrorInternalServerError("Template error sign_in_first.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

async fn confirm_email(
    request: HttpRequest,
    id: Identity,
    tmpl: web::Data<Tera>,
    users: configuration::WebConfig,
    //params: Option<web::Form<LoginParams>>,
    query: web::Query<HashMap<String, String>>,
) -> Result<HttpResponse, Error> {
    // Security so we don't get abused that easy by people that try to verify a users existence in the system
    let next = match query.get("next") {
        Some(n) if n.starts_with("/") => n,
        Some(_) | None => "/",
    };

    let mut users = users
        .lock()
        .expect("/me/confirm_email unable to lock users");
    let session = match users.session(&id) {
        None => {
            //if session.email.is_none()
            warn!(
                "me::confirm_email: session does not contain email <- {:?}",
                id.identity()
            );

            let candidate = if let Some(token) = query.get("token") {
                users.session_from_confirm_token(&id, token)
            } else {
                users.session(&id)
            };
            users.session_candidate_or_new(&id, candidate)
        }
        Some(session) => session,
    };

    // this should not work because the session object gets moved
    let mut ctx = default_tera_context(&session);
    let mut user: SaveGuard = match users.get_user_data_mut(session) {
        Some(user) => user,
        None => {
            warn!("me::confirm_email: unable to find user from session");
            id.forget();
            return Ok(HttpResponse::Found()
                .header("location", format!("/me/login?next={}", next))
                .finish());
        }
    };

    let mut messages: Vec<String> = vec![];
    let client_ip = client_ip_from_request(&request);

    debug!("me::confirm_email: user = {:?}", *user);

    if user.active_login_token.is_none() {
        let (ok, msg) = match user.start_login(client_ip).await {
            Ok(sr) => (true, format!("eMail sent: {:?}", sr)),
            Err(e) => (false, format!("unable to send eMail: {:?}", e)),
        };

        if ok == false {
            error!("me::confirm_email: {}", msg);
        }

        messages.push(msg);
        ctx.insert("email_sent", &ok);
    } else {
        let token = query.get("token");
        info!(
            "me::confirm_email: token = {:?} ?= {:?}",
            token, user.active_login_token
        );

        if token.is_some() && token == user.active_login_token.as_ref() {
            user.complete_login(client_ip);
            info!("me::confirm_email: email confirmed {}", user.email);
            return Ok(HttpResponse::Found().header("location", next).finish());
        } else {
            messages.push(format!("invalid token: {:?}", token));
        }
    }

    /*
        let mut new_email = true;

        use configuration::UserState::*;
        match user.state {
            Registered => {
                // new user
                new_email = true;
            }
            EMailSent(when) => {
                if when.elapsed() < Duration::from_secs(60 * 60) {
                    // resend the email after one hour
                    let msg = format!(
                        "Email sent to you {} ago",
                        compound_duration::format_dhms(when.elapsed().as_secs())
                    );
                    debug!("me::confirm_email: msg = {}", msg);
                    messages.push(msg);
                    new_email = false;
                }
            }
            Verified | TotpSecret(_) => {
                user.complete_login(client_ip);
                id.remember(email);
                return Ok(HttpResponse::Found().header("location", next).finish());
            }
        }
    */

    ctx.insert("next", next);
    ctx.insert("messages", &messages);

    let s = tmpl.render("confirm_email.html", &ctx).map_err(|e| {
        warn!("me::confirm_email::template: {:?}", e);
        error::ErrorInternalServerError("Template error confirm_email.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

async fn logout(
    request: HttpRequest,
    id: Identity,
    users: configuration::WebConfig,
) -> HttpResponse {
    let mut users = users
        .lock()
        .expect("/me/confirm_email unable to lock users");
    let session = users.session_or_default(&id);

    match session.state {
        SessionState::Unauthenticated => { /* no-op */ }
        SessionState::EMailSent | SessionState::LoggedIn => {
            if let Some(mut user) = users.get_user_data_mut(session) {
                let client_ip = client_ip_from_request(&request);
                user.logout(client_ip);
            }
        }
    }
    id.forget();
    HttpResponse::Found().header("location", "/").finish()
}

async fn me(
    //request: HttpRequest,
    id: Identity,
    tmpl: web::Data<Tera>,
    users: configuration::WebConfig,
) -> Result<HttpResponse, Error> {
    let mut users = users
        .lock()
        .expect("/me/confirm_email unable to lock users");
    let session = users.session_or_default(&id);

    if session.state != SessionState::LoggedIn {
        return Ok(HttpResponse::Found()
            .header("location", format!("/me/login?next=/me"))
            .finish());
    }
    let mut ctx = default_tera_context(&session);

    let user = users
        .get_user_data(
            &session
                .email
                .expect("all loggedin users have an email address"),
        )
        .expect("all loggedin users exist");
    ctx.insert("activities", &user.activity_report());

    let s = tmpl.render("me.html", &ctx).map_err(|e| {
        warn!("me: {:?}", e);
        error::ErrorInternalServerError("Template error me.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

pub async fn identicon(id: Identity, users: configuration::WebConfig) -> HttpResponse {
    let email = {
        let mut users = users
            .lock()
            .expect("/me/confirm_email unable to lock users");
        let session = users.session_or_default(&id);
        format!("{:?}", session.email)
    };

    let border = 16;
    let size = 8;
    let scale = 128;
    let background_color = (220, 220, 220);

    let identicon = identicon_rs::Identicon::new(&email)
        .border(border)
        .background_color(background_color)
        .size(size)
        .unwrap()
        .scale(scale)
        .unwrap();
    let file = identicon.export_png_data().unwrap();

    HttpResponse::Ok().content_type("image/png").body(file)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info,one_fa_auth=debug");
    env_logger::init();

    info!("starting version: {}", GIT_VERSION);

    //let bind_addr = std::env::var("BIND").unwrap_or_else(|_| "[::1]:8080".to_string());
    let bind_addr = get_prod_bind_addr()
        .or_else(|_| std::env::var("BIND"))
        .unwrap_or_else(|_| "[::1]:8080".to_string());
    info!("Binding to: http://{}/  (set BIND to overwrite)", bind_addr);

    info!(
        "Loading MailConfig for domain: {}",
        MAIL_CONFIG.creds.domain()
    );
    let configuration = configuration::restore()?;
    let config_auto_save = configuration.clone();

    HttpServer::new(move || {
        //let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*.html"))
        let tera = Tera::new("templates/**/*.html").expect("unable to load tera templates");

        // TODO dynamic
        let private_key: [u8; 32] = [
            0x12, 23, 23, 12, 3, 45, 2, 34, 6, 34, 0x66, 43, 6, 34, 57, 84, 82, 03, 91, 26, 38, 46,
            58, 76, 12, 42, 74, 29, 75, 29, 50, 72,
        ];

        let id = IdentityService::new(
            // <- create identity middleware
            CookieIdentityPolicy::new(&private_key) // <- create cookie identity policy
                .name("auth-cookie")
                .secure(true),
        );

        App::new()
            .data(tera)
            .data(configuration.clone())
            .wrap(id)
            .service(index)
            .service(
                actix_files::Files::new("/static/", "./static/")
                    .use_last_modified(true)
                    .use_etag(true)
                    .show_files_listing(),
            )
            .service(web::resource("/me").route(web::get().to(me)))
            .service(web::resource("/me/login").to(login))
            .service(web::resource("/me/confirm_email").to(confirm_email))
            .service(web::resource("/me/logout").to(logout))
            .service(web::resource("/me/identicon").route(web::get().to(identicon)))
        //.route("/ws/", web::get().to(ws_start))
    })
    .bind(bind_addr)?
    .run()
    .await?;

    info!("saving to disk");
    config_auto_save
        .lock()
        .expect("unable to save on shutdown")
        .store_to_disk()?;
    Ok(())
}

/*
#[derive(Serialize, Deserialize)]
pub struct UserContext {
    authenticated: bool,
    session: SessionId,
}
pub fn new_user_context(id: &Identity) -> UserContext {
    let (authenticated, session) = match id.get(SESSION_KEY_ID) {
        Some(session_id) => (true, SessionId(session_id)),
        None => (false, SessionId("Anonymous".to_owned())),
    };
    UserContext {
        authenticated,
        session,
    }
}
*/

pub fn default_tera_context(user: &UserSession) -> tera::Context {
    let mut ctx = tera::Context::new();

    ctx.insert("GIT_VERSION", GIT_VERSION);
    ctx.insert("session_state", &user.state);
    ctx.insert(
        "user_authenticated",
        &(user.state == configuration::SessionState::LoggedIn),
    );
    ctx.insert("user_email", &user.email);

    ctx
}

#[inline]
fn client_ip_from_request(request: &HttpRequest) -> IpAddr {
    let client_ip_str = request
        .connection_info()
        .realip_remote_addr()
        .unwrap_or("[::]:443")
        .to_string();
    client_ip_str
        .parse()
        .or_else(|_e| {
            info!("client_ip parse as addr failed: {:?}", _e);
            client_ip_str
                .parse::<SocketAddr>()
                .and_then(|sa| Ok(sa.ip()))
        })
        .expect(&*format!("unable to parse client_ip = {:?}", client_ip_str))
}

fn get_prod_bind_addr() -> std::io::Result<String> {
    let contents = std::fs::read_to_string("./backend_url.txt")?;

    Ok(extract_bind_addr(contents))
}
fn extract_bind_addr<S: AsRef<str>>(contents: S) -> String {
    contents
        .as_ref()
        .trim()
        .trim_start_matches("http://")
        .trim_end_matches("/")
        .to_string()
}

#[test]
fn test_get_prod_bind_addr() {
    assert_eq!("[::1]:5000", extract_bind_addr("http://[::1]:5000/\n"));
}
