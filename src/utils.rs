pub fn is_allowed_domain<S: AsRef<str>>(email: S) -> bool {
    let email = email.as_ref();
    if email.len() < 4 {
        return false;
    }

    let mut has_at = false;
    let mut has_name = false;
    let mut has_domain = false;
    let mut has_domain_dots = false;

    for c in email.chars() {
        if c == '@' {
            has_at = true;
            continue;
        }
        if has_at == false {
            has_name = true;
        } else {
            has_domain = true;
            if c == '.' {
                has_domain_dots = true;
            }
        }
    }

    has_name && has_at && has_domain && has_domain_dots
    // && email.ends_with("jku.at")
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn empty() {
        assert!(is_allowed_domain("") == false)
    }
    #[test]
    fn short() {
        assert!(is_allowed_domain("a@") == false)
    }
    #[test]
    fn no_at() {
        assert!(is_allowed_domain("students.jku.at") == false)
    }
    #[test]
    fn no_dots() {
        assert!(is_allowed_domain("a@studtents") == false)
    }
    #[test]
    fn employee() {
        assert!(is_allowed_domain("a@jku.at"))
    }
    #[test]
    fn student() {
        assert!(is_allowed_domain("a@students.jku.at"))
    }
}
