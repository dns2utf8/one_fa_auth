use lazy_static::lazy_static;
use log::trace;
use mailgun_v3::{
    email::{EmailAddress, Message, MessageBody, SendResponse},
    Credentials, MailgunResult,
};
use serde::Deserialize;

lazy_static! {
    pub static ref MAIL_CONFIG: MailConfig = load_from_file("./mail_config.json");
}

pub struct MailConfig {
    pub creds: Credentials,
    pub sender: EmailAddress,
}

#[derive(Debug, Deserialize)]
struct MailConfigLoader {
    base_url: String,
    token: String,
    domain: String,
    sender_name: String,
    sender_email: String,
}

fn load_from_file(path: &str) -> MailConfig {
    let file =
        std::fs::read_to_string(path).expect(&*format!("unable to load MailConfig from {}", path));

    let c: MailConfigLoader =
        serde_json::from_str(&file).expect(&*format!("unable to parse MailConfig from {}", path));
    trace!("{:?}", c);

    let mc = MailConfig {
        creds: Credentials::with_base(&c.base_url, &c.token, &c.domain),
        sender: EmailAddress::name_address(&c.sender_name, &c.sender_email),
    };

    mc
}
