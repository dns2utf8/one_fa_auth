'use strict';

const ws = new WebSocket((location.protocol.indexOf('https') === 0 ? 'wss://' : 'ws://') + location.host + '/ws/');

ws.addEventListener('open', function (event) {
    socket.send('Hello Server!');
});

ws.addEventListener('message', function (event) {
    console.log('Message from server ', event.data);
});

