//! A build script that never fails in finding a version for the build
//!
//! Also it will not trigger a rebuild unless the git version changes
//!
//! Author: Stefan Schindler <dns2utf8@estada.ch>

use std::process::Command;

fn main() {
    let git_version = Command::new("git")
        .arg("describe")
        .args(&["--tags", "--dirty"])
        .output();

    let git_version = git_version
        .map(|out| {
            if out.status.success() {
                String::from_utf8(out.stdout)
                    //.expect("Failed to decode git description")
                    .map_err(|_| ())
            } else {
                Err(())
            }
        })
        .unwrap_or(Err(()));
    let git_version = match git_version {
        Ok(s) => s,
        Err(_) => format!("{}-unknown-dirty", env!("CARGO_PKG_VERSION")),
    };
    println!("cargo:rustc-env=GIT_VERSION={}", git_version);
    println!("cargo:rerun-if-env-changed=GIT_VERSION={}", git_version);
}
