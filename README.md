# one_fa_auth

This is a PoC example so one can try this workflow:

1. Login with any email address
2. Click on the link that is sent to your account
3. Optional: Save the TOTP secret
4. Logout and Login with the TOTP

## Config

Copy `mail_config.example.json` to `mail_config.json` and fill in the fields.

## Server Setup with Docker installed

```
make systemd_setup
sudo make build_in_container && make deploy_direct 
```

## Sensitive Date

There are two files that you have to pay attention to:

1. `config.json` contains all your users with their eMail addresses
2. `mail_config.json` containing your credentials so anybody can send emails in your name
